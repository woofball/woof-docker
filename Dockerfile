FROM ubuntu:latest

# so we can avoid any prompts during updates
ENV DEBIAN_FRONTEND="noninteractive"

RUN apt-get update \
    &&  apt-get -y install wget xz-utils unzip git openjdk-11-jre python3 \
    && rm -rf /var/lib/apt/lists/*

# Setting up Flutter
RUN wget -q https://storage.googleapis.com/flutter_infra/releases/stable/linux/flutter_linux_1.17.5-stable.tar.xz \
    && tar xf ./flutter_linux_1.17.5-stable.tar.xz -C /opt \
    && rm ./flutter_linux_1.17.5-stable.tar.xz
ENV PATH="/opt/flutter/bin:$PATH"

# Setting up commandlinetools
RUN wget -q https://dl.google.com/android/repository/commandlinetools-linux-6200805_latest.zip \
    && mkdir /opt/android-clt \
    && unzip commandlinetools-linux-6200805_latest.zip -d /opt/android-clt \
    && rm commandlinetools-linux-6200805_latest.zip
ENV PATH="/opt/android-clt/tools/bin:$PATH"


# setting up android sdk
ENV ANDROID_HOME="/opt/android-clt/tools/bin"
RUN yes | sdkmanager --sdk_root=${ANDROID_HOME} --licenses \
    && sdkmanager --sdk_root=${ANDROID_HOME} "platforms;android-29" "build-tools;29.0.2"

RUN flutter --version \
    && flutter doctor
